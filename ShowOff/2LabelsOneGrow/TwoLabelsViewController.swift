//
//  TwoLabelsViewController.swift
//  ShowOff
//
//  Created by Marcela Nievas on 24/07/2018.
//  Copyright © 2018 Marce. All rights reserved.
//

import UIKit

class TwoLabelsViewController: UIViewController {
    
    @IBOutlet weak var label1: ConstrainedLabel!
    @IBOutlet weak var label2: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }

    @IBAction func b1(_ sender: Any) {
        label1.text = label1.text! + "1111 111 11 "
    }
    
    @IBAction func b2(_ sender: Any) {
        label2.text = label2.text! + "222 22 22 "
    }
 

    
}

