//
//  ConstraintedLabel.swift
//  ShowOff
//
//  Created by Marcela Nievas on 05/08/2018.
//  Copyright © 2018 Marce. All rights reserved.
//

import UIKit

class ConstrainedLabel: UILabel {

    override var intrinsicContentSize: CGSize {
        return CGSize(width: self.frame.width, height: 0)
    }
    
}
